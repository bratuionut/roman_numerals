from collections import OrderedDict
import re


def remove_space(string):
    return string.replace(" ", "")


def validate_input(string, valid_tokens):
    for char in list(string):
        if char not in valid_tokens:
            return False
    return True


def write_roman(num):
    roman = OrderedDict()
    roman[1000] = "M"
    roman[900] = "CM"
    roman[500] = "D"
    roman[400] = "CD"
    roman[100] = "C"
    roman[90] = "XC"
    roman[50] = "L"
    roman[40] = "XL"
    roman[10] = "X"
    roman[9] = "IX"
    roman[5] = "V"
    roman[4] = "IV"
    roman[1] = "I"

    def roman_num(num):
        for r in roman.keys():
            x, y = divmod(num, r)
            yield roman[r] * x
            num -= (r * x)
            if num <= 0:
                break

    return "".join([a for a in roman_num(num)])


def roman_to_integer(string):
    roman = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
    z = 0
    for i in range(0, len(string) - 1):
        if roman[string[i]] < roman[string[i + 1]]:
            z -= roman[string[i]]
        else:
            z += roman[string[i]]
    return z + roman[string[-1]]


def expression_string_to_arabic(expression_string):
    roman_numerals_tokens = ["I", "V", "X", "L", "C", "D", "M"]
    operation_tokens = ["(", ")", "^", "/", "*", "+", "-"]
    transformed_string = []
    collect_roman_tokens = False
    roman_number = []

    for char in expression_string:
        if char not in roman_numerals_tokens and collect_roman_tokens is True:
            joined_char = ''.join(roman_number)
            transformed_string.append(str(roman_to_integer(joined_char)))
            roman_number = []
            collect_roman_tokens = False
        if char in operation_tokens:
            transformed_string.append(char)
        if char in roman_numerals_tokens and collect_roman_tokens is False:
            collect_roman_tokens = True
            roman_number.append(char)
        elif char in roman_numerals_tokens and collect_roman_tokens is True:
            roman_number.append(char)
    if len(roman_number) > 0:
        joined_char = ''.join(roman_number)
        transformed_string.append(str(roman_to_integer(joined_char)))

    return ''.join(transformed_string)


def is_number(str):
    try:
        int(str)
        return True
    except ValueError:
        return False


def is_name(str):
    return re.match("\w+", str)


def peek(stack):
    return stack[-1] if stack else None


def apply_operator(operators, values):
    operator = operators.pop()
    right = values.pop()
    left = values.pop()
    values.append(eval("{0}{1}{2}".format(left, operator, right)))


def greater_precedence(op1, op2):
    precedences = {'+': 0, '-': 0, '*': 1, '/': 1}
    return precedences[op1] > precedences[op2]


def evaluate(expression):
    tokens = re.findall("[+/*()-]|\d+", expression)
    values = []
    operators = []
    for token in tokens:
        if is_number(token):
            values.append(int(token))
        elif token == '(':
            operators.append(token)
        elif token == ')':
            top = peek(operators)
            while top is not None and top != '(':
                apply_operator(operators, values)
                top = peek(operators)
            operators.pop()  # Discard the '('
        else:
            # Operator
            top = peek(operators)
            while top is not None and top not in "()" and greater_precedence(top, token):
                apply_operator(operators, values)
                top = peek(operators)
            operators.append(token)
    while peek(operators) is not None:
        apply_operator(operators, values)

    return values[0]


if __name__ == '__main__':
    roman_numerals_tokens = ["I", "V", "X", "L", "C", "D", "M"]
    operations_tokens = ["(", ")", "^", "/", "*", "+", "-"]
    valid_tokens = roman_numerals_tokens + operations_tokens
    sixty_seven = "LVIII+IX"
    expression_string = expression_string_to_arabic(sixty_seven)
    print(evaluate(expression_string))
