import unittest

from lexer import LexicalAnalyzer, Tag
from parser import Parser, AddOp, AstTree, SubOp
from interpreter import Interpreter


class TestFlow(unittest.TestCase):
    def setUp(self) -> None:
        self.lexer = LexicalAnalyzer()

    def test_lexer(self):
        tokens = self.lexer.lexical_scan("I + II")
        self.assertEqual(tokens[0].tag, Tag.ROMAN_NUM)
        self.assertEqual(tokens[0].value, 1)
        self.assertEqual(tokens[1].tag, Tag.ADDITION)
        self.assertEqual(tokens[2].tag, Tag.ROMAN_NUM)
        self.assertEqual(tokens[2].value, 2)

    def test_parser(self):
        tokens = self.lexer.lexical_scan("I + II")
        parser = Parser(tokens)
        tree = parser.build_tree()
        self.assertEqual(tree.left, 2)
        self.assertEqual(tree.right, 1)
        self.assertEqual(type(tree.op), AddOp)
        tokens = self.lexer.lexical_scan("I + (II - V)")
        parser = Parser(tokens)
        tree = parser.build_tree()
        self.assertEqual(type(tree.left), AstTree)
        self.assertEqual(tree.left.right, 2)
        self.assertEqual(type(tree.left.op), SubOp)

    def test_interpreter(self):
        tokens = self.lexer.lexical_scan("IV + IV * II / (I-V)")
        parser = Parser(tokens)
        ast_tree = parser.build_tree()
        evaluation = Interpreter(ast_tree)
        result = evaluation.result
        self.assertEqual(result, 36)
        tokens = self.lexer.lexical_scan("IV + IV ^ II ")
        parser = Parser(tokens)
        ast_tree = parser.build_tree()
        evaluation = Interpreter(ast_tree)
        result = evaluation.result
        self.assertEqual(result, 20)


