from parser import AstTree, NodeExpr
import copy


class ComputationStack:
    def __init__(self):
        self._stack = []

    def add_variable(self, value):
        self._stack.append(value)

    def evaluate(self):
        result = 0
        stack = self._stack
        first = stack.pop()
        op = stack.pop()
        second = stack.pop()
        result += op.compute()(first, second)

        while stack:
            op = stack.pop()
            value = stack.pop()
            result = op.compute()(result, value)
        return result


class Interpreter:
    def __init__(self, tree: AstTree):
        self.ast_tree = tree
        self.computation_stack = ComputationStack()

    @property
    def result(self):
        return self.walk(self.ast_tree)

    def walk(self, tree: AstTree):

        cmpt_stck = ComputationStack()

        cursor = tree
        while True:
            if isinstance(cursor.left, int):
                cmpt_stck.add_variable(cursor.right)
                cmpt_stck.add_variable(cursor.op)
                cmpt_stck.add_variable(cursor.left)
                break
            else:
                cmpt_stck.add_variable(cursor.right)
                cmpt_stck.add_variable(cursor.op)

            cursor = cursor.left

        res = cmpt_stck.evaluate()
        return res