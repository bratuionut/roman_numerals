For running the application 

docker build -t flask-image:latest .
docker run -d -p 5000:5000 flask-image

Make sure you have docker installed and port 5000 available

curl http://0.0.0.0:5000 should return  some html code