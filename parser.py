from lexer import Token, Num, Word, Tag, LexicalAnalyzer
from typing import List, Union, Callable, Optional
import operator


class Computation:
    def __init__(self, object_type: str):
        self.object_type = object_type

    def __call__(self):
        if self.object_type == "ADDITION":
            return operator.iadd
        if self.object_type == "MULTIPLICATION":
            return operator.imul
        if self.object_type == "POW":
            return operator.ipow
        if self.object_type == "SUBTRACTION":
            return operator.isub
        if self.object_type == "DIVISION":
            return operator.floordiv


class Op:
    def __init__(self, associative, compute: Callable):
        self.associative = associative
        self.compute = compute

    def __call__(self, right, left):
        if self.associative is True:
            return self.compute(right, left)
        elif self.associative is False:
            return self.compute(left, right)


class AddOp(Op):
    def __init__(self):
        self.type = "ADDITION"
        compute = Computation(self.type)
        super().__init__(True, compute)


class SubOp(Op):
    def __init__(self):
        self.type = "SUBTRACTION"
        compute = Computation(self.type)
        super().__init__(True, compute)


class MultOp(Op):
    def __init__(self):
        self.type = "MULTIPLICATION"
        compute = Computation(self.type)
        super().__init__(True, compute)


class DivOp(Op):
    def __init__(self):
        self.type = "DIVISION"
        compute = Computation(self.type)
        super().__init__(True, compute)


class PowOp(Op):
    def __init__(self):
        self.type = "POW"
        compute = Computation(self.type)
        super().__init__(False, compute)


class NodeExpr:
    def __init__(self, right, op, left):
        self.right = right
        self.left = left
        self.op = op


class AstTree:
    def __init__(self, right, left, op):
        self.op = op
        self.right = right
        self.left = left


class ShuntingYard:

    @staticmethod
    def apply_operator(operators, values):
        operators.pop()
        values.pop()
        values.pop()

    @staticmethod
    def peek(stack):
        return stack[-1] if stack else None

    @staticmethod
    def greater_precedence(op1, op2):
        precedences = {'+': 0, '-': 0, '*': 1, '/': 1, '^': 1}
        return precedences[op1.value] > precedences[op2.value]

    @staticmethod
    def build_values_subtree(operators, values):
        op_token = operators[-1]
        if op_token.tag == Tag.ADDITION:
            op = AddOp()
        elif op_token.tag == Tag.SUBTRACTION:
            op = SubOp()
        elif op_token.tag == Tag.MULTIPLICATION:
            op = MultOp()
        elif op_token.tag == Tag.DIVISION:
            op = MultOp()
        elif op_token.tag == Tag.POW:
            op = PowOp()

        right = values[-1].value
        left = values[-2].value
        values.append(None)
        # values.append(eval("{0}{1}{2}".format(left, operator, right)))

        return left, right, op

    def evaluate(self, tokens):
        values = []
        operators = []
        ordered_operations = []
        for token in tokens:
            if token.tag == Tag.ROMAN_NUM:
                values.append(token)
            elif token.tag == Tag.OPEN_PARANTHESIS:
                operators.append(token)
            elif token.tag == Tag.CLOSE_PARANTHESIS:
                top = self.peek(operators)
                while top is not None and top.tag != Tag.OPEN_PARANTHESIS:
                    ordered_operations.append(self.build_values_subtree(operators, values))
                    self.apply_operator(operators, values)
                    top = self.peek(operators)
                operators.pop()  # Discard the '('
            else:
                # Operator
                top = self.peek(operators)
                while top is not None and top.tag not in [Tag.CLOSE_PARANTHESIS,
                                                      Tag.OPEN_PARANTHESIS] and self.greater_precedence(top, token):
                    ordered_operations.append(self.build_values_subtree(operators, values))
                    self.apply_operator(operators, values)
                    top = self.peek(operators)
                operators.append(token)
        while self.peek(operators) is not None:
            ordered_operations.append(self.build_values_subtree(operators, values))
            self.apply_operator(operators, values)

        return ordered_operations


class Parser:
    def __init__(self, tokens: List[Token]):
        self.tokens = tokens

    def build_tree(self):

        shunting_yard = ShuntingYard()
        ordered_operations = shunting_yard.evaluate(self.tokens)

        operators_values = ordered_operations[::-1]
        tree = None
        while operators_values:
            node = operators_values.pop()
            if tree is None:
                tree = AstTree(node[0], node[1], node[2])
            else:
                tree = AstTree(node[0], tree, node[2])
        return tree
