import math


def is_square(number):
    squared = int(math.sqrt(number))
    if squared * squared == number:
        return True
    return False


def is_fibonnaci(number):
    if is_square(5 * number * number + 4) or is_square(5 * number * number - 4):
        return True
    return False


def pink_flamingo_number(number):
    result = None
    if is_fibonnaci(number) and number % 3 == 0 and number % 5 == 0:
        result = "Pink Flamingo"
    elif is_fibonnaci(number):
        result = "Flamingo"
    elif number % 3 == 0 and number % 5 == 0:
        result = "FizzBuzz"
    elif number % 3 == 0:
        result = "Fizz"
    elif number % 5 == 0:
        result = "Buzz"
    else:
        result = str(number)
    return result


def pink_flamingo(start, end):
    for i in range(start, end + 1):
        if i > 0:
            char = pink_flamingo_number(i)
            print(char)
        elif i == 0:
            print(str(0))


if __name__ == '__main__':
    pink_flamingo(0, 10000)
