import unittest
import json
import urllib.request


def post_data(url, data):
    data = urllib.parse.urlencode(data)
    data = data.encode("utf-8")
    req = urllib.request.Request(url, data)
    res = urllib.request.urlopen(req)
    res_body = res.read()
    j = json.loads(res_body.decode("utf-8"))
    return j


class TestWebApi(unittest.TestCase):
    def test_post_expression(self):
        d = dict()
        d["body"] = "LVIII+IX"
        res = post_data('http://127.0.0.1:5000', data=d)
        self.assertEqual(res['result'], 67)
