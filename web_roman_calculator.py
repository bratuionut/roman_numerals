import ast

from flask import Flask, request, render_template
from flask import jsonify

from interpreter import Interpreter
from lexer import LexicalAnalyzer
from parser import Parser

app = Flask(__name__)


def eval_expr(expression):
    lexer = LexicalAnalyzer()
    tokens = lexer.lexical_scan(expression)
    parser = Parser(tokens)
    ast_tree = parser.build_tree()
    evaluation = Interpreter(ast_tree)
    result = evaluation.result
    return result



@app.errorhandler(500)
def internal_server_error(error):
    app.logger.error('Server Error: %s', (error))
    return render_template('500.html'), 500


@app.route('/', methods=["POST", "GET"])
def roman_calculator():
    error = None
    if request.method == "POST":
        expression = request.form['body']
        d = dict()
        try:
            d["result"] = eval_expr(expression)
            return jsonify(d)
        except Exception as e:
            d["status_code"] = 500
            render_template("home.html", error=error)
    else:
        error = "Invalid expression input"
    return render_template("home.html", error=error)


