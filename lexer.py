from enum import Enum
from typing import List, Union

from roman_numeral_calculator import roman_to_integer


class Tag(Enum):
    EMPTY = 0,
    ROMAN_NUM = 1,
    OPEN_PARANTHESIS = 2,
    CLOSE_PARANTHESIS = 3,
    POW = 4,
    DIVISION = 5,
    MULTIPLICATION = 6,
    ADDITION = 7,
    SUBTRACTION = 8


class RomanNumber:
    ONE = 1
    FIVE = 2
    TEN = 3
    FIFTY = 4
    HUNDRED = 5
    FIVE_HUNDRED = 6
    ONE_THOUSAND = 7


class Token:
    def __init__(self, tag: Tag, value: Union[str, int]):
        self.tag = tag
        self.value = value

    def __str__(self):
        return "<" + str(self.tag.name) + ">"


class Word(Token):
    def __init__(self, tag: Tag, value: str):
        super().__init__(tag, value)

    def __str__(self):
        return str(self.tag) + "<>"+str(self.value)



class Num(Token):
    def __init__(self, tag: Tag, value: int):
        super().__init__(tag, value)
        self.value = value

    def __str__(self):
        return str(self.tag) + "<>"+str(self.value)


class SymbolTable:
    def __init__(self):
        self.operation_symbols = {
            "*": Tag.MULTIPLICATION,
            "(": Tag.OPEN_PARANTHESIS,
            ")": Tag.CLOSE_PARANTHESIS,
            "^": Tag.POW,
            "/": Tag.DIVISION,
            "+": Tag.ADDITION,
            "-": Tag.SUBTRACTION,
        }
        self.roman_symbols = {
            "I": RomanNumber.ONE,
            "V": RomanNumber.FIVE,
            "X": RomanNumber.TEN,
            "L": RomanNumber.FIFTY,
            "D": RomanNumber.HUNDRED,
            "M": RomanNumber.ONE_THOUSAND
        }
        self.reserved_characted = list(self.operation_symbols.keys()) + \
                                  list(self.roman_symbols.keys())

    def get_symbol(self, symbol: str):
        return self.operation_symbols[symbol]


class LexicalAnalyzer:
    def __init__(self):
        self.symbol_table = SymbolTable()

    def check_acceptable_chars(self, execution_string: str) -> bool:
        for char in execution_string:
            acceptable_chars = self.symbol_table.reserved_characted + [" ", "\n"]
            if char not in acceptable_chars:
                return False
        return True

    def clean_input(self, execution_string: str) -> str:
        execution_string = execution_string.replace(" ", "").replace("\n", "")
        return execution_string

    def translate_to_tokens(self, execution_string: str) -> List[Token]:
        roman_chars = list(self.symbol_table.roman_symbols.keys())
        operation_chars = list(self.symbol_table.operation_symbols.keys())
        tokens = []
        collect_roman_tokens = False
        roman_number = []

        for char in execution_string:
            if char not in roman_chars and collect_roman_tokens is True:
                joined_char = ''.join(roman_number)
                token = Num(Tag.ROMAN_NUM, roman_to_integer(joined_char))
                tokens.append(token)
                roman_number = []
                collect_roman_tokens = False
            if char in operation_chars:
                tag = self.symbol_table.operation_symbols[char]
                token = Word(tag, char)
                tokens.append(token)
            if char in roman_chars and collect_roman_tokens is False:
                collect_roman_tokens = True
                roman_number.append(char)
            elif char in roman_chars and collect_roman_tokens is True:
                roman_number.append(char)
        if len(roman_number) > 0:
            joined_char = ''.join(roman_number)
            token = Num(Tag.ROMAN_NUM, roman_to_integer(joined_char))
            tokens.append(token)

        return tokens

    def lexical_scan(self, execution_string: str) -> List[Token]:
        if self.check_acceptable_chars(execution_string) is True:
            execution_string = self.clean_input(execution_string)
            tokens = self.translate_to_tokens(execution_string)
            return tokens
        return [Token(Tag.EMPTY, 0)]