import unittest
from pink_flamingo import is_square, is_fibonnaci, pink_flamingo_number


class TestPinkFlamingo(unittest.TestCase):

    def test_is_square(self):
        self.assertTrue(is_square(4))
        self.assertFalse(is_square(5))

    def test_is_fibbonaci(self):
        self.assertTrue(is_fibonnaci(5))
        self.assertFalse(is_fibonnaci(4))

    def test_pink_flamingo_number(self):
        self.assertEqual(pink_flamingo_number(1), "Flamingo")
        self.assertEqual(pink_flamingo_number(15), "FizzBuzz")
        self.assertEqual(pink_flamingo_number(10), "Buzz")
        self.assertEqual(pink_flamingo_number(6), "Fizz")
        self.assertEqual(pink_flamingo_number(11), "11")


if __name__ == '__main__':
    unittest.main()
