import unittest

from roman_numeral_calculator import remove_space, validate_input, roman_to_integer, expression_string_to_arabic, \
    evaluate


class TestRomanNumeralCalculator(unittest.TestCase):

    def test_is_square(self):
        self.assertEqual(remove_space("a beautiful  day"), "abeautifulday")

    def test_validate_input(self):
        roman_numerals_tokens = ["I", "V", "X", "L", "C", "D", "M"]
        operations_tokens = ["(", ")", "^", "/", "*", "+", "-"]

        valid_tokens = roman_numerals_tokens + operations_tokens
        valid_input = remove_space("X*X + C")
        self.assertEqual(validate_input(valid_input, valid_tokens), True)
        self.assertEqual(validate_input("X*X + C", valid_tokens), False)
        self.assertEqual(validate_input("somechar", valid_tokens), False)

    def test_roman_to_integer(self):
        fifty_eight = "LVIII"
        nine = "IX"
        self.assertEqual(roman_to_integer(nine), 9)
        self.assertEqual(roman_to_integer(fifty_eight), 58)

    def test_expression_string_to_arabic(self):
        sixty_seven = "LVIII+IX"
        self.assertEqual(expression_string_to_arabic(sixty_seven), "58+9")

    def test_evaluate(self):
        self.assertEqual(evaluate("58+9"), 67)
