import ast

from roman_numeral_calculator import roman_to_integer


def is_roman_numeral(string):
    try:
        roman_to_integer(string)
        return True
    except:
        return False


def name_to_num(name, n):
    num = ast.copy_location(ast.Num(n=n), name)
    return num


class RomanNodeTransformer(ast.NodeTransformer):

    def generic_visit(self, node):
        super().generic_visit(node)
        if isinstance(node, ast.BitXor):
            node = ast.Pow()
        if isinstance(node, ast.Name) and is_roman_numeral(node.id):
            num = name_to_num(node, roman_to_integer(node.id))
            node = num
        return node


if __name__ == '__main__':
    expr = "expression = XI ^ III+II"
    tree = ast.parse(expr)
    optimizer = RomanNodeTransformer()
    tree = optimizer.visit(tree)
    print(ast.dump(tree))
    code = compile(tree, "<string>", "exec")
    eval(code)
    # Expression becomes a global variable compiled and evaluated at runtime
    print(expression)
