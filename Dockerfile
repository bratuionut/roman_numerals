FROM ubuntu:16.04
RUN apt-get update -y && apt-get install -y python3-pip
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip3 install -r requirements.txt
COPY . /app
COPY  web_roman_calculator.py /app/app.py
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
CMD ["flask", "run", "--host", "0.0.0.0"]